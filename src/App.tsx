import maplibregl from 'maplibre-gl';
import 'maplibre-gl/dist/maplibre-gl.css';

import {useState, useEffect} from 'react';
import Map, {Source, Layer, LayerProps} from 'react-map-gl';

const pointLayer: LayerProps = {
  id: 'point',
  type: 'circle',
  paint: {
    'circle-radius': 2,
    'circle-color': '#007cbf'
  }
};

export default function App() {
  const [allData, setAllData] = useState(null);
  const [pointCount, setPointCount] = useState(0)

  useEffect(() => {
    /* global fetch */
    fetch(
      '/address.json'
    )
      .then(resp => resp.json())
      .then(json => {
        if (json.features) {
          setPointCount(json.features.length);
        }
        setAllData(json)
      })
      .catch(err => console.error('Could not load data', err)); // eslint-disable-line
  }, []);

  return (
    <>
    <div style={{ padding: '1em' }}>
      <h3>Large geojson originally from https://samples.azuremaps.com/demos/large-geojson-files</h3>
      <h4>{`number of points: ${pointCount}`}</h4>
    </div>
      <Map
        initialViewState={{
          longitude: -105.0844,
          latitude: 40.5853,
          zoom: 12
        }}
        mapLib={maplibregl}
        style={{width: '100vw', height: '80vh'}}
        mapStyle="https://basemaps.cartocdn.com/gl/positron-gl-style/style.json"
      >
        {
          allData
            ? (
                <Source type="geojson" data={allData}>
                  <Layer {...pointLayer} />
                </Source>
              )
            : null
        }
      </Map>
    </>
  );
}
